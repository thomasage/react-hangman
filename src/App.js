import { useState } from 'react'
import Game from './Game'
import TryAgain from './TryAgain'
import './App.css'

const words = ['HELLO', 'BYE', 'BREAKFAST', 'DINNER', 'LUNCH']

function App () {
  const [mysteryWord, setMysteryWord] = useState(generateWord)
  const [score, setScore] = useState(0)
  const [usedLetters, setUsedLetters] = useState([])

  function generateWord () {
    return words[Math.floor(Math.random() * words.length)]
  }

  function handleLetterClick (letter) {
    if (usedLetters.includes(letter)) {
      setScore(score - 2)
      return
    }
    const newUsedLetters = [...usedLetters, letter]
    newUsedLetters.sort((a, b) => a.localeCompare(b))
    setUsedLetters(newUsedLetters)
    if (mysteryWord.includes(letter)) {
      setScore(score + 2)
    } else {
      setScore(score - 1)
    }
  }

  function handleRestart () {
    setMysteryWord(generateWord())
    setScore(0)
    setUsedLetters([])
  }

  let board = <Game mysteryWord={mysteryWord} onLetterClick={handleLetterClick} score={score} usedLetters={usedLetters}/>
  if (!mysteryWord.split('').map(letter => usedLetters.includes(letter) ? letter : '_').includes('_')) {
    board = <TryAgain onClick={handleRestart}/>
  }

  return (
        <div className="container">
            {board}
        </div>
  )
}

export default App
