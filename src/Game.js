import PropTypes from 'prop-types'
import Guess from './Guess'
import Keyboard from './Keyboard'
import Score from './Score'

function Game ({ mysteryWord, onLetterClick, score, usedLetters }) {
  return (
        <div>
            <Guess mysteryWord={mysteryWord} usedLetters={usedLetters}/>
            <Keyboard onLetterClick={onLetterClick} usedLetters={usedLetters}/>
            <Score score={score}/>
        </div>
  )
}

Game.propTypes = {
  mysteryWord: PropTypes.string.isRequired,
  onLetterClick: PropTypes.func.isRequired,
  score: PropTypes.number.isRequired,
  usedLetters: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
}

export default Game
