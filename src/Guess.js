import PropTypes from 'prop-types'

function Guess ({ mysteryWord, usedLetters }) {
  const displayWord = mysteryWord.split('').map(letter => usedLetters.includes(letter) ? letter : '_')
  return (
        <div className="d-flex flex-row mt-4">
            {displayWord.map((letter, index) =>
                <div key={index}
                     style={{ margin: '0 1em', padding: '0.5em 1em', border: '1px black solid' }}>
                  {letter}
                </div>
            )}
        </div>
  )
}

Guess.propTypes = {
  mysteryWord: PropTypes.string.isRequired,
  usedLetters: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
}

export default Guess
