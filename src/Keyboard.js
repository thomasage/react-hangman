import PropTypes from 'prop-types'
import Letter from './Letter'

function Keyboard ({ onLetterClick, usedLetters }) {
  const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')
  return (
        <div className="d-flex flex-row flex-wrap align-items-center justify-content-around mt-4">
            {alphabet.map(letter => (
                <Letter clicked={usedLetters.includes(letter)}
                        key={letter}
                        letter={letter}
                        onClick={onLetterClick}/>
            ))}
        </div>
  )
}

Keyboard.propTypes = {
  onLetterClick: PropTypes.func.isRequired,
  usedLetters: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
}

export default Keyboard
