import PropTypes from 'prop-types'

function Letter ({ clicked, letter, onClick }) {
  const className = `m-2 btn btn-lg ${clicked ? 'btn-primary' : 'btn-outline-secondary'}`
  return (
        <button type="button"
                className={className}
                style={{ width: '6%' }}
                onClick={() => onClick(letter)}>
            {letter}
        </button>
  )
}

Letter.defaultValues = {
  clicked: false
}
Letter.propTypes = {
  clicked: PropTypes.bool,
  letter: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default Letter
