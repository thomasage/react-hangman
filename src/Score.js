import PropTypes from 'prop-types'

function Score ({ score }) {
  return (
        <div className="mt-5">Score : {score}</div>
  )
}

Score.propTypes = {
  score: PropTypes.number.isRequired
}

export default Score
