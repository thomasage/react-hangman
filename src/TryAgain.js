import PropTypes from 'prop-types'

function TryAgain ({ onClick }) {
  return (
        <div className="mt-5">
            <div className="alert alert-success">Well done!</div>
            <div className="mt-5 text-center">
                <button type="button" className="btn btn-primary btn-lg" onClick={onClick}>Restart</button>
            </div>
        </div>
  )
}

TryAgain.propTypes = {
  onClick: PropTypes.func.isRequired
}

export default TryAgain
